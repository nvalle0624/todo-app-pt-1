//following Kano walkthrough
//import useState for hooks in functional component
import React, { useState } from "react";
//use different variable name
import { todos as todosList } from "./todos";
//id generator
import { v4 as uuid } from "uuid";

//changed to functinoal component to use hooks
function App() {
  //destructure array [variable name, then function]
  const [todos, setTodos] = useState(todosList);
  //same for input text
  const [inputText, setInputText] = useState("");

  //event handler for text input
  const handleAddTodo = (event) => {
    //for when 'enter' key is pressed, key value of 13
    if (event.which === 13) {
      const newId = uuid();
      const newTodo = {
        userId: 1,
        id: newId,
        title: inputText,
        completed: false,
      };
      const newTodos = { ...todos };
      newTodos[newId] = newTodo;
      setTodos(newTodos);
      setInputText("");
    }
  };

  const handleToggle = (id) => {
    //copy todos
    const newTodos = { ...todos };
    //toggle state of 'completed' to the opposite boolean state for item whose id was passed in with the click
    newTodos[id].completed = !newTodos[id].completed;
    setTodos(newTodos);
  };

  const handleDeleteTodo = (id) => {
    const newTodos = { ...todos };
    //delete todo with key of 'id' that is passed
    delete newTodos[id];
    //reset list
    setTodos(newTodos);
  };

  const handleClearCompleted = () => {
    const newTodos = { ...todos };
    //loop through newTodos, for each todo, if 'completed'= true, then delete that todo
    for (const todo in newTodos) {
      if (newTodos[todo].completed) {
        delete newTodos[todo];
      }
    }
    setTodos(newTodos);
  };
  //no need for render method
  return (
    <section className="todoapp">
      <header className="header">
        <h1>todos</h1>
        <input
          onChange={(event) => setInputText(event.target.value)}
          onKeyDown={(event) => handleAddTodo(event)}
          value={inputText}
          className="new-todo"
          placeholder="What needs to be done?"
          autofocus
        />
      </header>
      <TodoList todos={Object.values(todos)} handleToggle={handleToggle} handleDeleteTodo={handleDeleteTodo} />
      <footer className="footer">
        <span className="todo-count">
          <strong>0</strong> item(s) left
        </span>
        <button className="clear-completed" onClick={() => handleClearCompleted()}>
          Clear completed
        </button>
      </footer>
    </section>
  );
}

function TodoItem(props) {
  return (
    <li className={props.completed ? "completed" : ""}>
      <div className="view">
        <input
          className="toggle"
          type="checkbox"
          checked={props.completed}
          onChange={() => props.handleToggle(props.id)}
        />
        <label>{props.title}</label>
        <button className="destroy" onClick={() => props.handleDeleteTodo(props.id)} />
      </div>
    </li>
  );
}

function TodoList(props) {
  return (
    <section className="main">
      <ul className="todo-list">
        {props.todos.map((todo) => (
          <TodoItem
            title={todo.title}
            completed={todo.completed}
            id={todo.id}
            handleToggle={props.handleToggle}
            key={todo.id}
            handleDeleteTodo={props.handleDeleteTodo}
          />
        ))}
      </ul>
    </section>
  );
}

export default App;
